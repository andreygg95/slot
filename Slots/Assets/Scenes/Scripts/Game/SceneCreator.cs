﻿using System.Collections;
using System.Linq;
using Slot.Settings;
using UnityEngine;

namespace Slot.Game
{
    public class SceneCreator : MonoBehaviour
    {
        public MoveSettings moveSettings;
        public ScreenSettings screenSettings;


        private int columnsCount => screenSettings.columnsCount;
        private int rowsCount => screenSettings.rowsCount;
        private Vector2 iconSize => screenSettings.iconSize;
        private Vector2 iconSpaces => screenSettings.iconSpaces;

        private static int RandomIconIndex => Random.Range(0, 8);

        private bool isMoveEnabled;

        private LineScroller[] lines;

        private void Awake()
        {
            lines = new LineScroller[columnsCount];
            var parent = new GameObject("ParentForSlots");
            var data = CalculateData(iconSize.y, iconSpaces.y, rowsCount, GetIconPosition(rowsCount).y);

            for (int i = 0; i < columnsCount; i++)
            {
                lines[i] = CreateLine(i, data);
                lines[i].transform.parent = parent.transform;

                lines[i].movingCurve = moveSettings.moveCurve;
                lines[i].blurCurve = moveSettings.blurCurve;
            }

            StartCoroutine(StartMoving(1f));
        }

        private void OnGUI()
        {
            if (isMoveEnabled && GUI.Button(new Rect(0, 0, Screen.width / 10, Screen.height / 10), "MoveAgain"))
                StartCoroutine(StartMoving(0));
        }

        private LineScroller CreateLine(int index, ScreenSizeData data)
        {
            Icon[] icons = new Icon[rowsCount + 1];
            var parent = new GameObject($"Column_{index}").transform;
            parent.position = GetColumnPosition(index);

            for (int i = 0; i <= rowsCount; i++)
            {
                int iconIndex = RandomIconIndex;
                var icon = Icon.CreateIcon($"Icon_{index}_{i}", iconSize, iconIndex);
                icon.transform.parent = parent;
                var position = GetIconPosition(i);
                icon.transform.localPosition = position;
                icon.startYPos = position.y;
                icon.screenData = data;
                icons[i] = icon;
            }

            var line = parent.gameObject.AddComponent<LineScroller>();
            line.icons = icons;

            return line;
        }

        private Vector3 GetColumnPosition(int index)
        {
            float sumSize = iconSize.x + iconSpaces.x;
            float startPos = -(columnsCount / 2f - .5f) * sumSize;

            return Vector3.right * (startPos + index * sumSize);
        }

        private Vector3 GetIconPosition(int index)
        {
            float sumSize = iconSize.y + iconSpaces.y;
            float startPos = -(rowsCount / 2f - .5f) * sumSize;

            return Vector3.up * (startPos + index * sumSize);
        }

        private IEnumerator StartMoving(float delay)
        {
            isMoveEnabled = false;

            yield return new WaitForSeconds(delay);

            for (int i = 0; i < lines.Length; i++)
                lines[i].StartMoving(i * moveSettings.columnDelay);

            yield return new WaitForSeconds(lines.Length * moveSettings.columnDelay +
                                            moveSettings.moveCurve.keys.Last().time);
            isMoveEnabled = true;
        }

        private static ScreenSizeData CalculateData(float iconHeight, float spaceHeight, int count, float maxIconHeight)
        {
            var screenData = new ScreenSizeData();
            screenData.iconSumHeight = iconHeight + spaceHeight;
            screenData.columnHeight = screenData.iconSumHeight * (count + 1);
            screenData.minYPos = maxIconHeight - screenData.columnHeight;

            return screenData;
        }
    }
}