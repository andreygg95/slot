﻿using System.Collections;
using UnityEngine;

namespace Slot.Game
{
    public class LineScroller : MonoBehaviour
    {
        public AnimationCurve movingCurve;
        public AnimationCurve blurCurve;
        public Icon[] icons;

        public void StartMoving(float delay) => StartCoroutine(Moving(delay));

        private IEnumerator Moving(float delay)
        {
            yield return new WaitForSeconds(delay);

            float moveTime = movingCurve.keys[movingCurve.length - 1].time;
            float t = 0;

            while (t < moveTime)
            {
                t += Time.deltaTime;
                var position = movingCurve.Evaluate(t);
                SetIconPosition(position);
                var blurValue = blurCurve.Evaluate(t);
                SetIconBlur((int) blurValue);

                yield return new WaitForEndOfFrame();
            }

            SetIconPosition(movingCurve.Evaluate(moveTime));
            SetIconBlur((int) blurCurve.Evaluate(moveTime));
            StopIcons();
        }

        private void SetIconPosition(float position)
        {
            foreach (var icon in icons)
                icon.SetPosition(position);
        }

        private void SetIconBlur(int blurValue)
        {
            foreach (var icon in icons)
                icon.SetBlurValue(blurValue);
        }

        private void StopIcons()
        {
            foreach (var icon in icons)
                icon.Stop();
        }
    }
}