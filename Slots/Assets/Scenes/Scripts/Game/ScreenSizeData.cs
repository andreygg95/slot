﻿using UnityEngine;

namespace Slot.Game
{
    public class ScreenSizeData
    {
        public float iconSumHeight;
        public float minYPos;
        public float columnHeight;
    }
}