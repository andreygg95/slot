﻿using UnityEngine;
using static Slot.Tools.AssetRuntimeGenerator;

namespace Slot.Game
{
    public class Icon : MonoBehaviour
    {
        public float startYPos;
        public int iconIndex;
        public ScreenSizeData screenData;

        private Material material;
        private int lastRoundedValue;

        private static int RandomIconIndex => Random.Range(0, 8);

        public static Icon CreateIcon(string name, Vector2 iconSize, int iconIndex)
        {
            var gameObject = new GameObject(name);

            var icon = gameObject.AddComponent<Icon>();
            icon.iconIndex = iconIndex;
            icon.material = GetMaterial(IconsTextureContainer.Instance.GetIcon(iconIndex, 0));

            gameObject.AddComponent<MeshRenderer>().sharedMaterial = icon.material;
            gameObject.AddComponent<MeshFilter>().sharedMesh = GetMesh(iconSize);
            return icon;
        }

        public void SetBlurValue(int value) =>
            material.mainTexture = IconsTextureContainer.Instance.GetIcon(iconIndex, value);

        public void SetPosition(float value)
        {
            float yPos = startYPos + value * screenData.iconSumHeight;
            transform.localPosition = new Vector3(0, ClampedPos(yPos));
        }

        private float ClampedPos(float value)
        {
            float delta = value - screenData.minYPos;
            delta /= screenData.columnHeight;
            int roundedValue = RoundToMin(delta);

            if (lastRoundedValue != roundedValue)
            {
                lastRoundedValue = roundedValue;
                ChangeTexture();
            }

            return value - roundedValue * screenData.columnHeight;
        }

        private void ChangeTexture() => iconIndex = RandomIconIndex;

        public void Stop()
        {
            lastRoundedValue = 0;
            startYPos = transform.localPosition.y;
        }

        private static int RoundToMin(float value)
        {
            int x = (int) value;
            if (Mathf.Approximately(x, value)) return x;
            if (value < 0) return --x;

            return x;
        }
    }
}