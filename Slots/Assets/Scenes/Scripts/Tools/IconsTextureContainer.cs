﻿using UnityEngine;
using Slot.Settings;

namespace Slot.Tools
{
    public class IconsTextureContainer
    {
        public static IconsTextureContainer Instance => _instance = _instance ?? new IconsTextureContainer();
        private static IconsTextureContainer _instance;

        private const string AssetPath = "Scriptables/icon_";

        private const int iconsCount = 8;
        private const int maxBlurLevel = 5;

        private IconsTextureContainer() => icons = new Texture[iconsCount, maxBlurLevel];

        private Texture[,] icons;

        public Texture GetIcon(int iconIndex, int blurLevel)
        {
            if (icons[iconIndex, blurLevel] == null)
                LoadTextures(iconIndex);

            return icons[iconIndex, blurLevel];
        }

        private void LoadTextures(int iconIndex)
        {
            var asset = Resources.Load<IconLinks>(AssetPath + (iconIndex + 1));
            for (int i = 0; i < asset.textures.Length; i++)
                icons[iconIndex, i] = asset.textures[i];
        }
    }
}