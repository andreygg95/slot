﻿using UnityEngine;

namespace Slot.Tools
{
    public static class AssetRuntimeGenerator
    {
        public static Mesh GetMesh(Vector2 size)
        {
            float sizex = size.x / 2;
            float sizey = size.y / 2;
            return new Mesh
            {
                vertices = new[]
                {
                    new Vector3(-sizex, -sizey), new Vector3(-sizex, sizey), new Vector3(sizex, sizey),
                    new Vector3(sizex, -sizey)
                },
                uv = new[] {Vector2.zero, Vector2.up, Vector2.one, Vector2.right},
                triangles = new[] {0, 1, 2, 0, 2, 3}
            };
        }

        public static Material GetMaterial(Texture texture) =>
            new Material(Shader.Find("Unlit/Transparent")) {mainTexture = texture};
    }
}