﻿using UnityEngine;

namespace Slot.Settings
{
    [CreateAssetMenu]
    public class ScreenSettings : ScriptableObject
    {
        public int columnsCount;
        public int rowsCount;

        public Vector2 iconSize;
        public Vector2 iconSpaces;
    }
}