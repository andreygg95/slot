﻿using UnityEngine;

namespace Slot.Settings
{
    [CreateAssetMenu]
    public class MoveSettings : ScriptableObject
    {
        public AnimationCurve moveCurve;
        public AnimationCurve blurCurve;
        public float columnDelay;
    }
}