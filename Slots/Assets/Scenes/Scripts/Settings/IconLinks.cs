﻿using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Slot.Settings
{
    [CreateAssetMenu]
    public class IconLinks : ScriptableObject
    {
        public Texture2D[] textures;

#if UNITY_EDITOR
        [MenuItem("Tools/CreateLink #_1")]
        public static void CreateLink()
        {
            var asset = new IconLinks();
            asset.textures = Selection.GetFiltered<Texture2D>(SelectionMode.DeepAssets).OrderBy(x => x.name).ToArray();
            AssetDatabase.CreateAsset(asset, $"Assets/{asset.textures[0].name.Substring(0, 6)}.asset");
        }
#endif
    }
}